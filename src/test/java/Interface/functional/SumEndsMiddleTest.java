package Interface.functional;

import Interface.function.DivEqu;
import Interface.function.FunctionReal;
import Interface.function.LinEqu;
import Interface.function.SinEqu;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SumEndsMiddleTest {
    @Test
    public void testBoundsLin(){
        SumEndsMiddle func = new SumEndsMiddle(0,1);
        FunctionReal lin = new LinEqu(1,1,-1,2);
        assertEquals(4.5,func.functional(lin),0.1);
    }
    @Test
    public void testBoundsDiv(){
        SumEndsMiddle func = new SumEndsMiddle(0,1);
        FunctionReal div = new DivEqu(1,1,1,1,-1,2);
        assertEquals(3.0,func.functional(div),0.1);
    }
    @Test
    public void testBoundsSin(){
        SumEndsMiddle func = new SumEndsMiddle(0,1);
        FunctionReal sin = new SinEqu(1,1,-1,2);
        assertEquals(1.3,func.functional(sin),0.1);
    }

}
