package Interface.functional;

import org.junit.Test;
import Interface.function.DivEqu;
import Interface.function.FunctionReal;
import Interface.function.LinEqu;
import Interface.function.SinEqu;

import static org.junit.Assert.assertEquals;

public class IntegralFuncTest {
    @Test
    public void testFunctionalLin() {
        IntegralFunc fun = new IntegralFunc(0, 1);
        FunctionReal lin = new LinEqu(1, 0, 0, 1);
        assertEquals(0.5, fun.functional(lin), 0.1);
    }

    @Test
    public void testFunctionalDiv() {
        IntegralFunc fun = new IntegralFunc(0, 1);
        FunctionReal div = new DivEqu(1, 0, 0, 1, 0, 1);
        assertEquals(0.5, fun.functional(div), 0.1);
    }

    @Test
    public void testFunctionalSin() {
        IntegralFunc fun = new IntegralFunc(0, Math.PI/2.);
        FunctionReal sin = new SinEqu(2, 1, 0, 4);
        System.out.println(fun.functional(sin));
        //assertEquals(2,fun.functional(sin), 0.1);
        assertEquals(2,fun.functional(sin), 1e-3);
    }
}
