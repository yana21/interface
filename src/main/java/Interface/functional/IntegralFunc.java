package Interface.functional;

import Interface.function.FunctionReal;

public class IntegralFunc implements FunOfOneArg {
    private double leftEnd;
    private double rightEnd;

    public IntegralFunc(double leftEnd, double rightEnd) {
        this.leftEnd = leftEnd;
        this.rightEnd = rightEnd;
    }

    public double functional(FunctionReal func) {
        double h = (rightEnd - leftEnd) / 10000;
        double rez = 0;
        for (double i = 0; i < 10000; i ++) {
            rez += func.getValueAtPoint(leftEnd + h*i)*h;
        }
        return rez;
    }
}
