package Interface.functional;

import Interface.function.FunctionReal;

public class SumEndsMiddle implements FunOfOneArg {
    private double leftBound;
    private double rightBound;

    public SumEndsMiddle(double leftBound, double rightBound) {
        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }

    @Override
    public double functional(FunctionReal func) {
        if (func.getLeft() > leftBound  || rightBound > func.getRight()) {
            throw new IndexOutOfBoundsException();
        }
        return func.getValueAtPoint(leftBound) + func.getValueAtPoint(rightBound) + func.getValueAtPoint((rightBound + leftBound) / 2);
    }
}
