package Interface.functional;

import Interface.function.FunctionReal;

public interface FunOfOneArg <T extends FunctionReal>{
    double functional(T func);
}
