package Interface.function;

public interface FunctionReal {
    double getValueAtPoint(double x);
    double getLeft();
    double getRight();
}
