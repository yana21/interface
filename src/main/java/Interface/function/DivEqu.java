package Interface.function;

public class DivEqu implements FunctionReal {
    //f(x) = (Ax+b)/(Cx+d);
    private double a;
    private double b;
    private double c;
    private double d;
    private double left;
    private double right;

    public DivEqu(double a, double b, double c, double d, double left, double right) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.left = left;
        this.right = right;
    }

    public double getValueAtPoint(double x) {
        if (c * x + d <= 10E-9 || x > right || x < left) {
            throw new RuntimeException();
        } else {
            return (a * x + b) / (c * x + d);
        }
    }

    @Override
    public double getLeft() {
        return left;
    }

    @Override
    public double getRight() {
        return right;
    }
}
